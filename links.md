
![image](https://github.com/nu11secur1ty/Puppet/blob/master/photo/Puppet's_company_logo.png)


------------------------------------------------------------------------------------------------------------
![image](https://github.com/nu11secur1ty/Puppet/blob/master/photo/puppet-labs-logo.png)

1. # Installing Puppet: Red Hat Enterprise Linux (and Derivatives)
   https://docs.puppet.com/puppet/3.8/install_el.html

2. # Installing Puppet: Pre-Install Tasks
   https://docs.puppet.com/puppet/3.8/pre_install.html

3. # Installing Puppet: Post-Install Tasks
   https://docs.puppet.com/puppet/3.8/post_install.html

4. # Installing Puppet agent: Linux
   https://docs.puppet.com/puppet/latest/install_linux.html

5. # Installing and Configuring the Puppet Agent Red Hat
   https://access.redhat.com/documentation/en-US/Red_Hat_Satellite/6.0/html/User_Guide/Installing_and_Configuring_the_Puppet_Agent.html


  
